// categories-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const categories = new Schema({
    name: {
      required: true,
      type: String,
      unique: true,
      minlength: 2,
      trim: true
    },
    description: {
      type: String,
      default: 'no description',
      trim: true,
      minlength: 2
    }
  }, {
      timestamps: true
    });

  return mongooseClient.model('categories', categories);
};
