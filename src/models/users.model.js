// users-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
const validator = require('validator');

module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const users = new mongooseClient.Schema({

    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: {
        validator: validator.isEmail,
        message: 'It is not email!'
      },
      minlength: 1,
      trim: true
    },
    password: {
      type: String,
      required: true,
      validate: {
        validator: (v) => {
          return validator.isLength(v, 6, undefined);
        },
        message: 'Too short password'
      }
    }

  }, {
      timestamps: true
    });

  return mongooseClient.model('users', users);
};
