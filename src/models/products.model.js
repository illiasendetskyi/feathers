// products-model.js - A mongoose model
// 
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
const validator = require('validator');
// const mongoose = require('mongoose');
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const products = new Schema({
    name: {
      required: true,
      type: String,
      unique: true
    },
    categoryId: {
      type: String,
      required: true,
      validate: {
        validator: (categoryId) => validator.isMongoId(categoryId),
        message: 'Not valid categoryId'
      }
    },
    properties: {
      type: String,
      default: 'no properties',
      trim: true,
      minlength: 2
    },
    cost: {
      required: true,
      type: Number,
      default: 0,
      trim: true
    }
  }, {
      timestamps: true
    });

  return mongooseClient.model('products', products);
};
