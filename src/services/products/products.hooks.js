const { authenticate } = require('@feathersjs/authentication').hooks;

const validateCategoryId = require('../../hooks/validate-category-id');

module.exports = {
  before: {
    all: [],
    find: [validateCategoryId()],
    get: [validateCategoryId()],
    create: [validateCategoryId(), authenticate('jwt')],
    update: [validateCategoryId(), authenticate('jwt')],
    patch: [validateCategoryId(), authenticate('jwt')],
    remove: [validateCategoryId(), authenticate('jwt')]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
