// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
const errors = require('@feathersjs/errors');
const validator = require('validator');
const _ = require('lodash');
module.exports = function (options = {}) {
  return async context => {
    if(!_.isEmpty(context.params.query) && !validator.isMongoId(context.params.query.categoryId)) {
      return Promise.reject(new errors.BadRequest('Invalid categoryId', {
        categoryId: context.params.query.categoryId
      }));
    }    
    return context;
  };
};
