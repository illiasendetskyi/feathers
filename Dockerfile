FROM node:carbon

LABEL maintainer="Illia Sendetskyi <illiasendetskyi@gmail.com>"

WORKDIR /feathers_app

COPY . .

RUN npm install

EXPOSE 3000 3030

CMD ["npm", "start"]