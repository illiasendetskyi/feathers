const supertest = require('supertest');
const test = require('ava');
const jwt = require('jsonwebtoken');
const app = require('./../app');
const JWT_SECRET = require('./../../config/default.json').authentication.secret;
const {signUp, delUsers} = require('./../_helpers/users_helpers');
const {signupUser, testUser, expiredToken} = require('./../_helpers/users_helpers');

const request = supertest(app);
let token = null;
test.before.serial(signUp);
test.before.serial(async () => {
    const res = await request
        .post('/authentication')
        .send({
            "email": signupUser.email,
            "password": signupUser.password,
            "strategy": 'local'
        });
    token = res.body.accessToken;
    return token;
});
test.after.always(delUsers);
test('\'POST /users\' should create a user with valid data and return created user back', async t => {
    const res = await request
        .post('/users')
        .send(testUser);
    t.is(res.status, 201);
    t.deepEqual(res.body.name, testUser.name);
});

test('\'POST /authentication\', should login with valid data and get back response with valid JWT', async t => {
    const res = await request
        .post('/authentication')
        .send({
            "email": signupUser.email,
            "password": signupUser.password,
            "strategy": 'local'
        });
    t.is(res.status, 201);
    t.deepEqual(jwt.verify(res.body.accessToken, JWT_SECRET).userId, signupUser._id.toHexString());
});
test('\'GET /me\', should send back the authenticated user if token is valid', async t => {
    const res = await request
        .get('/me')
        .set('Authorization', token);
    t.is(res.status, 200);
    t.deepEqual(res.body._id, signupUser._id.toHexString());
});
test('\'POST /users\', should not sign up with duplicate email', async t => {
    const res = await request
        .post('/users')
        .send(signupUser);
    t.is(res.status, 409);
});
test('\'POST /users\', should not sign up with invalid name', async t => {
    const res = await request
        .post('/users')
        .send({
            name: '',
            email: 'example@email.com',
            password: 'abc123'
        });
    t.is(res.status, 400);
});
test('\'POST /users\', should not sign up with invalid email', async t => {
    const res = await request
        .post('/users')
        .send({
            name: 'name',
            email: 'invalid_email',
            password: 'abc123'
        });
    t.is(res.status, 400);
});
test('\'POST /users\', should not sign up with too short password', async t => {
    testUser.password = 'abc12';
    const res = await request
        .post('/users')
        .send(testUser);
    t.is(res.status, 400);
});
test('\'POST /authentication\', should not login with invalid password', async t => {
    const res = await request
        .post('/authentication')
        .send({
            email: signupUser.email,
            password: 'invalid_password',
            "strategy": 'local'
        });
    t.is(res.status, 401);
});
test('\'POST /authentication\', should not login with invalid email', async t => {
    const res = await request
        .post('/authentication')
        .send({
            email: 'invalid_email',
            password: signupUser.password,
            "startegy": "local"
        });
    t.is(res.status, 401);
});
test('\'GET /me\', should return 401 if token is expired', async t => {
    const res = await request
        .get('/me')
        .set('Authorization', expiredToken);
    t.is(res.status, 401);
});
test('\'GET /me\', should return 401 if token is invalid', async t => {
    const res = await request
        .get('/me')
        .set('Authorization', 'invalidToken123');
    t.is(res.status, 401);
});