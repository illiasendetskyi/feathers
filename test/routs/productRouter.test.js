const supertest = require('supertest');
const test = require('ava');
const mongoose = require('mongoose');
const app = require('./../app');
const {addProducts, delProducts} = require('./../_helpers/products_helpers');
const {addCategoriesForProducts, delCategoriesForProducts} = require('./../_helpers/products_helpers');
const {categoriesForProducts, products, testProduct} = require('./../_helpers/products_helpers');
const {signUp, delUsers, expiredToken, signupUser} = require('./../_helpers/users_helpers');

const request = supertest(app);
let token = null;
test.before.serial(signUp);
test.before.serial(async () => {
    const res = await request
        .post('/authentication')
        .send({
            "email": signupUser.email,
            "password": signupUser.password,
            "strategy": 'local'
        });
    token = res.body.accessToken;
    return token;
});
test.before(addCategoriesForProducts);
test.before(addProducts);
test.after.always(delProducts);
test.after.always(delCategoriesForProducts);
test.after.always(delUsers);
test('\'GET /products\' should return all products from test database if query param is empty', async t => {
    const res = await request
        .get('/products')
        .set('Authorization', token);
    t.is(res.status, 200);
    t.deepEqual(true, res.body.data.length >= 3);
});
test('\'GET /products/?categoryId=\' should return products from test database by categoryId query param', async t => {
    const res = await request
        .get(`/products/?categoryId=${categoriesForProducts[0]._id}`)
        .set('Authorization', token);
    t.is(res.status, 200);
    t.deepEqual(true, res.body.data.length >= 1);
    t.deepEqual(res.body.data[0].categoryId, categoriesForProducts[0]._id.toHexString());
});
test('\'GET /products/?categoryId=\' should return an empty array if category is empty', async t => {
    const res = await request
        .get(`/products/?categoryId=${categoriesForProducts[2]._id.toHexString()}`)
        .set('Authorization', token);
    t.is(res.status, 200);
    t.deepEqual(res.body.data.length, 0);
});
test('\'GET /products/?categoryId=\' should return 400 if categoryId is not valid', async t => {
    const res = await request
        .get(`/products/?categoryId=${mongoose.Types.ObjectId().toHexString()}` + '1')
        .set('Authorization', token);
    t.is(res.status, 400);
});
test('\'POST /products\' should post product if token and data are valid and return this product. ' +
    'And add \'no properties\' if \'properties\' field is empty', async t => {
    const res = await request
        .post(`/products`)
        .set('Authorization', token)
        .send(testProduct);
    t.is(res.status, 201);
    t.deepEqual(res.body._id, testProduct._id.toHexString());
    t.deepEqual(res.body.properties, 'no properties');
});
test('\'POST /products/:categoryId\' should return 401 if user unauthorized.', async t => {
    const res = await request
        .post(`/products`)
        .set('Authorization', expiredToken);
    t.is(res.status, 401);
});
test('\'POST /products\' should return 400 if categoryId is invalid', async t => {
    testProduct.categoryId += '1'; 
    const res = await request
        .post(`/products`)
        .set('Authorization', token)
        .send(testProduct);
    t.is(res.status, 400);
});
test('\'POST /products/:categoryId\' should return 400 if data is invalid.', async t => {
    const res = await request
        .post(`/products`)
        .set('Authorization', token)
        .send({
            name: '',
            properties: '',
            cost: '',
            categoryId: products[0].categoryId.toHexString()
        });
    t.is(res.status, 400);
});
test('\'PATCH /products/:_id\' should update product if token and data are valid and return updated product', async t => {
    const res = await request
        .patch(`/products/${products[0]._id.toHexString()}`)
        .set('Authorization', token)
        .send({
            cost: 999
        });
    t.is(res.status, 200);
    t.deepEqual(res.body._id, products[0]._id.toHexString());
});
test('\'PATCH /products/:_id\' should return 401 if user unauthorized.', async t => {
    const res = await request
        .patch(`/products/${products[0]._id.toHexString()}`)
        .set('Authorization', expiredToken)
        .send();
    t.is(res.status, 401);
});
test('\'PATCH /products/:_id\' should return 404 if product id not found', async t => {
    const res = await request
        .patch(`/products/${mongoose.Types.ObjectId().toHexString()}`)
        .set('Authorization', token)
        .send();
    t.is(res.status, 404);
});
test('\'PATCH /products/:_id\' should return 400 if product id is invalid', async t => {
    const res = await request
        .patch(`/products/${mongoose.Types.ObjectId().toHexString()}` + '1')
        .set('Authorization', token)
        .send();
    t.is(res.status, 400);
});
test('\'PATCH /products/:_id\' should return 400 if data is invalid.', async t => {
    const res = await request
        .patch(`/products/${categoriesForProducts[0]._id.toHexString()}`)
        .set('Authorization', token)
        .send({
            name: '',
            properties: '',
            cost: ''
        });
    t.is(res.status, 400);
});
test('\'DELETE /products/:_id\' should delete product if token and data are valid and return deleted product', async t => {
    const res = await request
        .delete(`/products/${products[1]._id.toHexString()}`)
        .set('Authorization', token)
        .send();
    t.is(res.status, 200);
    t.deepEqual(res.body._id, products[1]._id.toHexString());
});
test('\'DELETE /products/:_id\' should return 401 if user unauthorized.', async t => {
    const res = await request
        .delete(`/products/${products[1]._id.toHexString()}`)
        .set('Authorization', expiredToken)
        .send();
    t.is(res.status, 401);
});
test('\'DELETE /products/:_id\' should return 404 if product id not found', async t => {
    const res = await request
        .delete(`/products/${mongoose.Types.ObjectId().toHexString()}`)
        .set('Authorization', token)
        .send();
    t.is(res.status, 404);
});
test('\'DELETE /products/:_id\' should return 400 if product id is invalid', async t => {
    const res = await request
        .delete(`/products/${mongoose.Types.ObjectId().toHexString()}` + '1')
        .set('Authorization', token)
        .send();
    t.is(res.status, 400);
});