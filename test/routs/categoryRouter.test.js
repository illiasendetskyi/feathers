const supertest = require('supertest');
const test = require('ava');
const mongoose = require('mongoose');
const app = require('./../app');
const { addCategories, delCategories } = require('./../_helpers/categories_helpers');
const { signUp, delUsers, expiredToken, signupUser } = require('./../_helpers/users_helpers');
const { testCategory, categories } = require('./../_helpers/categories_helpers');

const request = supertest(app);
let token = null;
test.before.serial(signUp);
test.before.serial(async () => {
    const res = await request
        .post('/authentication')
        .send({
            "email": signupUser.email,
            "password": signupUser.password,
            "strategy": 'local'
        });
    token = res.body.accessToken;
    return token;
});
test.before(addCategories);
test.after.always(delCategories);
test.after.always(delUsers);
test('\'GET /categories\' should return all categories from test db', async t => {
    const res = await request
        .get('/categories')
        .set('Authorization', token);
    t.is(res.status, 200);
    t.deepEqual(true, res.body.data.length >= 3);
});
test('\'POST /categories\' should post category if token and data are valid and return this category back. And ' +
    'add \'no description\' if \'description\' field is empty', async t => {
        const res = await request
            .post('/categories')
            .set('Authorization', token)
            .send(testCategory);
        t.is(res.status, 201);
        t.deepEqual(res.body._id, testCategory._id.toHexString());
        t.deepEqual(res.body.description, 'no description');
    });
test('\'PATCH /categories/:id\' should update category if data and token are valid and ' +
    'return updated category', async t => {
        const res = await request
            .patch(`/categories/${categories[0]._id.toHexString()}`)
            .set('Authorization', token)
            .send({ description: 'new description' });
        t.is(res.status, 200);
        t.deepEqual(res.body._id, categories[0]._id.toHexString());
        t.deepEqual(res.body.description, 'new description');
    });
test('\'DELETE /categories/:id\' should delete category if data and token are valid and return ' +
    'deleted category', async t => {
        const res = await request
            .delete(`/categories/${categories[2]._id.toHexString()}`)
            .set('Authorization', token);
        t.is(res.status, 200);
        t.deepEqual(res.body._id, categories[2]._id.toHexString());
    });
test('\'POST /categories\' should return 401 if user unauthorized', async t => {
    const res = await request
        .post('/categories')
        .set('Authorization', expiredToken);
    t.is(res.status, 401);
});
test('\'POST /categories\' should not create category if invalid data', async t => {
    const res = await request
        .post('/categories')
        .set('Authorization', token)
        .send({
            name: "",
            description: ""
        });
    t.is(res.status, 400);
});
test('\'PATCH /categories/:id\' should return 401 if user unauthorized', async t => {
    const res = await request
        .patch(`/categories/${categories[0]._id.toHexString()}`)
        .set('Authorization', expiredToken)
        .send();
    t.is(res.status, 401);
});
test('\'PATCH /categories/:id\' should not update category if data invalid', async t => {
    const res = await request
        .patch(`/categories/${categories[0]._id.toHexString()}`)
        .set('Authorization', token)
        .send({
            name: "",
            description: ""
        });
    t.is(res.status, 400);
});
test('\'PATCH /categories/:id\' should return 404 if category doesn\'t find by id', async t => {
    const res = await request
        .patch(`/categories/${mongoose.Types.ObjectId().toHexString()}`)
        .set('Authorization', token)
        .send({
            name: "new_name",
            description: "new_description"
        });
    t.is(res.status, 404);
});
test('\'PATCH /categories/:id\' should return 400 if id invalid', async t => {
    const res = await request
        .patch(`/categories/${mongoose.Types.ObjectId().toHexString()}` + '1')
        .set('Authorization', token)
        .send({
            name: "new_name",
            description: "new_description"
        });
    t.is(res.status, 400);
});
test('\'DELETE /categories/:id\' should return 401 if user unauthorized.', async t => {
    const res = await request
        .delete(`/categories/${categories[1]._id.toHexString()}`)
        .set('Authorization', expiredToken)
        .send();
    t.is(res.status, 401);
});
test('\'DELETE /categories/:id\' should return 400 if id invalid', async t => {
    const res = await request
        .delete(`/categories/${categories[1]._id.toHexString()}` + '1')
        .set('Authorization', token)
        .send();
    t.is(res.status, 400);
});
test('\'DELETE /categories/:id\' should return 404 if category doesn\'t find by id', async t => {
    const res = await request
        .delete(`/categories/${mongoose.Types.ObjectId().toHexString()}`)
        .set('Authorization', token)
        .send();
    t.is(res.status, 404);
});
