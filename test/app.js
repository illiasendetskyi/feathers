const app = require('./../src/app');
const appHooks = require('./app.hooks');
app.hooks(appHooks);

app.setup(app);
module.exports = app;