const app = require('./../../src/app');
const mongoose = require('mongoose');
const randomstring = require('randomstring');

const categories = [{
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5),
    description: randomstring.generate(7)
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
}];
const testCategory = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
};
const addCategories = async () => {
    await app.service('categories').create(categories);
};
const delCategories = async () => {
    await app.service('categories').remove([
        categories[0]._id,
        categories[1]._id,
        categories[2]._id,
        testCategory._id
    ]);
};

module.exports = { addCategories, delCategories, testCategory, categories };