const mongoose = require('mongoose');
const randomstring = require('randomstring');
const app = require('./../../src/app');

const categoriesForProducts = [{
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5),
    description: randomstring.generate(7)
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(5)
}];
const products = [{
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[0]._id,
    description: randomstring.generate(8),
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[0]._id,
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
}, {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[1]._id,
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
}];
const testProduct = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6),
    categoryId: categoriesForProducts[1]._id,
    cost: randomstring.generate({
        length: 4,
        charset: 'numeric'
    })
};
const addProducts = async () => {
    await app.service('products').create(products);
};
const delProducts = async () => {
    await app.service('products').remove([
        products[0]._id,
        products[1]._id,
        products[2]._id,
        testProduct._id
    ]);
};
const addCategoriesForProducts = async () => {
    await app.service('categories').create(categoriesForProducts);
};
const delCategoriesForProducts = async () => {
    await app.service('categories').remove([
        categoriesForProducts[0]._id,
        categoriesForProducts[1]._id,
        categoriesForProducts[2]._id
    ]);
};

module.exports = {
    addProducts, delProducts,
    products, testProduct, addCategoriesForProducts, delCategoriesForProducts, categoriesForProducts
};