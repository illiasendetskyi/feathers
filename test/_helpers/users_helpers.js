const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const randomstring = require('randomstring');
const JWT_SECRET = require('./../../config/default.json').authentication.secret;
const app = require('./../../src/app');

const signupUser = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6) + '_signUpUser',
    email: randomstring.generate(6) + '_signUpUser@email.com',
    password: 'abc123'
};
const testUser = {
    _id: mongoose.Types.ObjectId(),
    name: randomstring.generate(6) + '_testUser',
    email: randomstring.generate(6) + '@email.com',
    password: randomstring.generate(6)
};

const expiredToken = jwt.sign({_id: signupUser._id}, JWT_SECRET, {expiresIn: 0});
const signUp = async () => {
    await app.service('users').create(signupUser);
};
const delUsers = async () => {
    await app.service('users').remove([signupUser._id, testUser._id]);
};

module.exports = {signUp, delUsers, signupUser, testUser, expiredToken};